package com.pfe.web;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pfe.dao.ContactRepository;
import com.pfe.entities.Contact;

@CrossOrigin("*")
@RestController

@RequestMapping("/api")
public class ContactRestService {
	@Autowired
	private ContactRepository contactRepository;

	@GetMapping("/contacts")
	public List<Contact> getContacts() {
		return contactRepository.findAll();
	}

	@GetMapping("/contacts/{id}")
	public Optional<Contact> getContact(@PathVariable Long id) {
		return contactRepository.findById(id);
	}

	@PostMapping("/contacts")
	public Contact saveContact(@RequestBody Contact c) {
		return contactRepository.save(c);
	}

	@DeleteMapping("/contacts/{id}")
	public boolean deleteContact(@PathVariable Long id) {
		contactRepository.deleteById(id);
		return true;
	}

	@PutMapping("/contacts/{id}")
	public void updateContact(@PathVariable Long id, @RequestBody Contact c) {
		c.setId(id);
		contactRepository.save(c);
	}

	@GetMapping("/contacts/recherche")
	public Page<Contact> chercher(
			@RequestParam(name = "mc", defaultValue = "") String mc,
			@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "size", defaultValue = "5") int size) {
		return contactRepository.chercher("%"+mc+"%", PageRequest.of(page, size));
	}
}
