package com.pfe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.pfe.dao.ContactRepository;
import com.pfe.entities.Contact;

@SpringBootApplication
public class ContactProjectApplication implements CommandLineRunner {
	@Autowired
	private ContactRepository contactRepository;

	public static void main(String[] args) {
		SpringApplication.run(ContactProjectApplication.class, args);
	}
	@Bean
    public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/api").allowedOrigins("http://localhost:4200");
			}
		};
    }
	@Override
	public void run(String... args) throws Exception {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		contactRepository
				.save(new Contact("Molka", "Moalla", df.parse("04/09/1997"), "molka@gmail.com", 50368108, "molka.png"));
		contactRepository
				.save(new Contact("Mohamed", "Moalla", df.parse("27/02/1982"), "med@gmail.com", 50012215, "med.png"));
		contactRepository.save(new Contact("Ahmed", "Ben Salah", df.parse("09/01/1999"), "abensalah@gmail.com",
				22558896, "ahmed.png"));
	}

}
